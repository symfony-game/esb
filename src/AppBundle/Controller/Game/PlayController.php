<?php

namespace AppBundle\Controller\Game;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PlayController extends Controller
{
    /**
     * @Route("/game/new.json")
     */
    public function newAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/api/doc")
     */
    public function apiDocAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/{direction}/play.json")
     */
    public function playAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/boards/{board}.json")
     */
    public function getBoardAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    /**
     * @Route("/game/boards.json")
     */
    public function getBoardsAction(Request $request)
    {
        return $this->proxyCall($request);
    }

    private function proxyCall(Request $request)
    {
        $client = $this->get('csa_guzzle.client.game');

        return new Response($client->request($request->getMethod(), $request->getRequestUri(), $request->request->all())->getBody()->getContents());
    }
}
